import React from 'react';
import Authorization from "./components/Authorization";

function App() {
  return (
    <div className="App">
        <Authorization/>
    </div>
  );
}

export default App;

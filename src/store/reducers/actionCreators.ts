import * as AuthActionCreators from './auth/actions'

export default {
    ...AuthActionCreators,
}
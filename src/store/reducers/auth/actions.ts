import {
    authActionTypes,
    logInFailureAction,
    logInStartAction,
    logInSuccessAction
} from "../../../types/auth";

export const logInStart = (email: string, password: string, setSubmitting: (isSubmitting: boolean) => void,
                           setStatus: (status?: any) => void,
                           setFieldError: (field: string, message: string | undefined) => void): logInStartAction => ({
    type: authActionTypes.AUTH_REQUEST,
    payload: {email, password, setSubmitting, setStatus, setFieldError}
});

export const logInSuccess = (token: string): logInSuccessAction => ({
    type: authActionTypes.AUTH_SUCCESS,
    payload: {token},
});

export const logInFailure = (error: string): logInFailureAction => ({
    type: authActionTypes.AUTH_FAILURE,
    payload: {error},
});







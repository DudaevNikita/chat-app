import {authAction, authActionTypes} from "../../../types/auth";

const INITIAL_STATE = {
    currentUser: null,
    token: null,
    loading: false,
    error: null,

};

const authReducer = (state = INITIAL_STATE, action: authAction) => {
    switch (action.type) {
        case authActionTypes.AUTH_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case authActionTypes.AUTH_SUCCESS:
            return {
                ...state,
                token: action.payload.token,
                error: null,
                loading: false,
            };
        case authActionTypes.AUTH_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                loading: false,
            };
        default:
            return state;
    }
};




export default authReducer;
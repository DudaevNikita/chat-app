import { takeLatest, put, call} from 'redux-saga/effects';
import {getAuth, signInWithEmailAndPassword} from "firebase/auth";
import {authActionTypes, logInStartAction} from "../../../types/auth";
import {logInFailure, logInSuccess} from "../../reducers/auth/actions";


export const signInAuthUserWithEmailAndPassword = async(email: string, password: string) => {
    const auth = getAuth();
    if (!email || !password) return;

    return await signInWithEmailAndPassword(auth, email, password)

}

function* startsAuthSaga(action:logInStartAction) {

    try {
        // @ts-ignore
        const result = yield call(signInAuthUserWithEmailAndPassword,
            action.payload.email,
            action.payload.password
        )
        yield put(logInSuccess(result.user.uid));
        action.payload.setSubmitting(false)
    } catch (error:any) {
        yield put(logInFailure(error.message));
        if (error.code === 'auth/wrong-password') {
            action.payload.setFieldError('password', error.message)
        }
        if (error.code === 'auth/user-not-found') {
            action.payload.setFieldError('email', error.message)
        }
        if ( (error.code !== 'auth/wrong-password') && (error.code !== 'auth/user-not-found')) {
            action.payload.setStatus(error.message)
        }
        action.payload.setSubmitting(false)
    }

}

export default function* authSaga() {
    yield takeLatest(authActionTypes.AUTH_REQUEST, startsAuthSaga);
}
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { initializeApp } from "firebase/app";
import {Provider} from "react-redux";
import {store} from './store'

const firebaseConfig = {
    apiKey: "AIzaSyAW0Jw4wDdOUUJhTxNo4vw1rR2Hlrmb45c",
    authDomain: "chat-app-b2793.firebaseapp.com",
    projectId: "chat-app-b2793",
    storageBucket: "chat-app-b2793.appspot.com",
    messagingSenderId: "430616694564",
    appId: "1:430616694564:web:2a5618b0c003d586172db1",
    measurementId: "G-G07L6QNGZP"
}

const app = initializeApp(firebaseConfig);


const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);

root.render(
    <Provider store={store}>
        <App/>
    </Provider>

);

import React from 'react';
import {Box, Button, LinearProgress, Paper, Typography} from '@mui/material';
import {Field, Form, Formik, FormikHelpers,} from 'formik';
import {TextField} from 'formik-mui';
import * as Yup from 'yup';
import {useActions} from "../hooks/useActions";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faUser} from "@fortawesome/free-solid-svg-icons";

interface Values {
    email: string;
    password: string;
}

function Authorization() {
    const {logInStart} = useActions()
    const login = (values:Values, {setSubmitting, setStatus, setFieldError}: FormikHelpers<Values>) => {
        const {email, password} = values
        setStatus('')
        logInStart(email, password, setSubmitting, setStatus, setFieldError)
    }

    return (
        <Box sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100vh'
        }}>
            <Paper elevation={3} sx={{p: 10, width: '200px'}}>
                <Box sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingBottom: '40px',
                }}>
                    <FontAwesomeIcon icon={faUser} size="lg"/>
                    <Typography variant="h4" component="h1" sx={{pl: '10px'}}>
                        Регистрация
                    </Typography>
                </Box>

                <Formik
                    initialValues={{
                        email: '',
                        password: '',
                    }}
                    validationSchema={Yup.object({
                        email: Yup.string().email('Invalid email address').required('Required'),
                        password: Yup.string().required('Required'),
                    })}
                    onSubmit={login}
                >


                    {({submitForm, isSubmitting, status}) => (
                        <Form>
                            {!!status && <Typography sx={{color: 'red'}}>{status}</Typography>}
                            <Field
                                component={TextField}
                                name="email"
                                type="email"
                                label="Email"
                                size="small"
                                sx={{mb: 1}}

                            />
                            <br/>
                            <Field
                                component={TextField}
                                type="password"
                                label="Password"
                                name="password"
                                size="small"
                                sx={{mb: 1}}
                            />
                            {isSubmitting && <LinearProgress/>}
                            <br/>
                            <Button
                                sx={{width: '100%'}}
                                variant="contained"
                                color="primary"
                                disabled={isSubmitting}
                                onClick={submitForm}

                            >
                                Submit
                            </Button>
                        </Form>
                    )}
                </Formik>
            </Paper>
        </Box>

    );
}

export default Authorization;
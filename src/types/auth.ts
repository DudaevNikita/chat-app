export enum authActionTypes {
    AUTH_REQUEST = 'AUTH_REQUEST',
    AUTH_SUCCESS = 'AUTH_SUCCESS',
    AUTH_FAILURE = 'AUTH_FAILURE',
}

export interface logInStartAction {
    type: authActionTypes.AUTH_REQUEST,
    payload: {email: string, password:string, setSubmitting: (isSubmitting: boolean) => void,
        setStatus: (status?: any) => void,
        setFieldError: (field: string, message: string | undefined) => void },
}

export interface logInSuccessAction {
    type: authActionTypes.AUTH_SUCCESS,
    payload: { token: string },
}

export interface logInFailureAction {
    type: authActionTypes.AUTH_FAILURE,
    payload: { error: string },
}

export type authAction = logInStartAction | logInSuccessAction | logInFailureAction
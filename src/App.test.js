import {shallow} from "enzyme";
import App from "./App";

    it('component contains class ".App"', () => {
        const component  = shallow(
            <App />
        )
        const wrapper = component.find('.App')
        expect(wrapper.length).toBe(1)
    })

